import org.apache.commons.lang3.math.NumberUtils;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class MergeInt {
    private static boolean ascend;
    private static String filenameOutput;
    private static String[] filenameInput;
    private static int MAX_VALUE;
    private static int MIN_VALUE;
    private static int BOUND_VALUE_INT;
    private static int MAX_LENGTH_STRING = 200; //Максимальное кол-во символов, помещающихся в строку, после которых
                                                //строка добавляется в файл

    static void init() {
        ascend = MergeSort.ascend;
        filenameOutput = MergeSort.filenameOutput;
        filenameInput = MergeSort.filenameInput;
        MAX_VALUE = MergeSort.MAX_VALUE_INT;
        MIN_VALUE = MergeSort.MIN_VALUE_INT;
        BOUND_VALUE_INT = MergeSort.BOUND_VALUE_INT;
    }

    private static int compare(int[] buffer) {
        if (ascend) {
            return min(buffer);
        } else {
            return max(buffer);
        }
    }
    private static int min(int[] buffer) {
        int min = MAX_VALUE, indexMin = 0;
        for (int i = 0; i < buffer.length; i++) {
            if (buffer[i] < min) {
                min = buffer[i];
                indexMin = i;
            }
        }
        return indexMin;
    }

    private static int max(int[] buffer) {
        int max = MIN_VALUE, indexMax = 0;
        for (int i = 0; i < buffer.length; i++) {
            if (buffer[i] > max) {
                max = buffer[i];
                indexMax = i;
            }
        }
        return indexMax;
    }

    private static int nextValue(Scanner[] scanners, int value, int lastValue, int[] buffer){
        String tmpStr;
        buffer[value] = BOUND_VALUE_INT;
        while (scanners[value].hasNext()) {
            tmpStr = scanners[value].next();
            if(NumberUtils.isNumber(tmpStr)) {
                if(ascend){
                    if(lastValue <= Integer.parseInt(tmpStr)){
                        buffer[value] = Integer.parseInt(tmpStr);
                        break;
                    }
                }else {
                    if(lastValue >= Integer.parseInt(tmpStr)){
                        buffer[value] = Integer.parseInt(tmpStr);
                        break;
                    }
                }
            }
        }
        return buffer[value];
    }

     static void mergeSortInt() {
        Scanner[] scanners = new Scanner[filenameInput.length];
        boolean[] accessToFile = new boolean[filenameInput.length];
        File[] inputFile = new File[filenameInput.length];
        int[] buffer = new int[filenameInput.length];
        String result = "";
        String tmpStr = "";
        try(FileWriter out = new FileWriter(filenameOutput)) {
            for (int i = 0; i < filenameInput.length; i++) {
                buffer[i] = BOUND_VALUE_INT;
                inputFile[i] = new File(filenameInput[i]);
                if (inputFile[i].canRead()) {
                    accessToFile[i] = true;
                }else{
                    System.out.println("Ошибка при открытии файла \"" + inputFile[i] + "\", пропуск.");
                    accessToFile[i] = false;
                }
            }
            for (int i = 0; i < filenameInput.length; i++) {
                if (accessToFile[i]) {
                    scanners[i] = new Scanner(inputFile[i]);
                }
            }
            for (int i = 0; i < filenameInput.length; i++) {
                if (accessToFile[i]) {
                    while (scanners[i].hasNext()) {
                        tmpStr = scanners[i].next();
                        if (NumberUtils.isNumber(tmpStr)) {
                            buffer[i] = Integer.parseInt(tmpStr);
                            break;
                        }
                    }
                }
            }

            int value = 0;
            while (true) {
                value = compare(buffer);
                result += buffer[value] + "\n";
                if(result.length() >= MAX_LENGTH_STRING){
                    out.write(result);
                    result = "";
                }
                int lastValue = buffer[value];
                buffer[value] = nextValue(scanners, value, lastValue, buffer);

                boolean flag_exit = true;
                for (int i = 0; i < scanners.length; i++) {
                    if (accessToFile[i]) {
                        if (scanners[i].hasNext()) {
                            flag_exit = false;
                            break;
                        }
                    }
                }
                if (flag_exit) {
                    while (true) {
                        value = compare(buffer);
                        if (buffer[value] != BOUND_VALUE_INT) {
                            result += buffer[value] + "\n";
                            buffer[value] = BOUND_VALUE_INT;
                        } else {
                            break;
                        }
                    }
                    break;
                }
            }
            out.write(result);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
