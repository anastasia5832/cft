public class MergeSort {
    static boolean ascend = true;
    static String filenameOutput;
    static String[] filenameInput;
    static final int MAX_VALUE_INT = 99999999;
    static final int MIN_VALUE_INT = -9999999;
    static int BOUND_VALUE_INT = MAX_VALUE_INT;
    static String BOUND_VALUE_STRING;
    private static boolean typeInt;

    private static void printInstruction(){
        System.out.println("При запуске программы необходимо ввести опции:" +
                "\nРежим сортировки (-a или -d), необязательный, по умолчанию сортируем по возрастанию" +
                "\nТип данных (-s или -i), обязательный;" +
                "\nИмя выходного файла, обязательное" +
                "\nИмена входных файлов, не менее одного." +
                "\nПример:" +
                "\nMergeSort.exe -a -i out.txt in1.txt  (для целых чисел по возрастанию)" +
                "\nИспользуйте системную кодировку");
    }

    private static void parseArgs(String[] args) {
        if(args[0].equals("-h")){
            printInstruction();
            System.exit(0);
        }
        int positionArgs = 0;
        if (args[positionArgs].equals("-a")) {
            positionArgs++;
            ascend = true;
            BOUND_VALUE_INT = MAX_VALUE_INT;
            BOUND_VALUE_STRING = "";
            char temp = 255;
            for(int i = 0; i < 10; i++) {
                BOUND_VALUE_STRING += Character.toString(temp);
            }
        } else if (args[positionArgs].equals("-d")) {
            positionArgs++;
            ascend = false;
            BOUND_VALUE_INT = MIN_VALUE_INT;
            BOUND_VALUE_STRING = "";
            char temp = 31;
            for(int i = 0; i < 10; i++) {
                BOUND_VALUE_STRING += Character.toString(temp);
            }
        }

        if (args[positionArgs].equals("-s")) {
            positionArgs++;
            typeInt = false;
        } else if (args[positionArgs].equals("-i")) {
            typeInt = true;
            positionArgs++;
        } else {
            System.out.println("Error! Invalid args " + args[positionArgs] + ". Use \"-i\" or \"-s\".");
            System.exit(-1);
        }

        filenameOutput = args[positionArgs];

        filenameInput = new String[args.length - positionArgs - 1];
        for (int i = 0; i < args.length - positionArgs - 1; i++) {
            filenameInput[i] = args[i + positionArgs + 1];
        }
    }

    public static void main(String[] args) {

        parseArgs(args);
        if(typeInt){
            MergeInt.init();
            MergeInt.mergeSortInt();
        } else {
            MergeString.init();
            MergeString.mergeSortString();
        }
    }
}