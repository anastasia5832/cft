import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class MergeString {
    private static boolean ascend;
    private static String filenameOutput;
    private static String[] filenameInput;
    private static String MAX_VALUE;
    private static String MIN_VALUE;
    private static int MAX_LENGTH_STRING = 200; //Максимальное кол-во символов, помещающихся в строку, после которых
                                                //строка добавляется в файл


    static void init(){
        ascend = MergeSort.ascend;
        filenameOutput = MergeSort.filenameOutput;
        filenameInput = MergeSort.filenameInput;
    }

    private static int compare(String[] buffer) {
        if (ascend) {
            return min(buffer);
        } else {
            return max(buffer);
        }
    }

    private static int min(String [] buffer) {
        String min = null;
        int indexMin = 0;
        for(int i = 0; i < buffer.length; i++) {
            if (buffer[i] != null) {
                min = buffer[i];
                indexMin = i;
                break;
            }
        }
        for (int i = 0; i < buffer.length; i++) {
            if (buffer[i] != null) {
                if (buffer[i].compareTo(min) < 0) {
                    min = buffer[i];
                    indexMin = i;
                }
            }
        }
        return indexMin;
    }

    private static int max(String[] buffer) {
        String max = null;
        int indexMax = 0;
        for (int i = 0; i < buffer.length; i++) {
            if (buffer[i] != null) {
                max = buffer[i];
                indexMax = i;
                break;
            }
        }
        for (int i = 0; i < buffer.length; i++) {
            if (buffer[i] != null) {
                if (buffer[i].compareTo(max) > 0) {
                    max = buffer[i];
                    indexMax = i;
                }
            }
        }
        return indexMax;
    }

    private static String nextValue(Scanner[] scanners, int value, String lastValue, String[] buffer){
        String tmpStr;
        buffer[value] = null;
        while (scanners[value].hasNext()) {
            tmpStr = scanners[value].nextLine();
            if (ascend) {
                if (lastValue.compareTo(tmpStr) <= 0) {
                    buffer[value] = tmpStr;
                    break;
                }
            } else {
                if (lastValue.compareTo(tmpStr) >= 0) {
                    buffer[value] = tmpStr;
                    break;
                }
            }
        }
        return buffer[value];
    }
    static void mergeSortString() {
        Scanner[] scanners = new Scanner[filenameInput.length];
        boolean[] accessToFile = new boolean[filenameInput.length];
        File[] inputFile = new File[filenameInput.length];
        String[] buffer = new String[filenameInput.length];
        String result = "";
        try(FileWriter out = new FileWriter(filenameOutput)) {
            for (int i = 0; i < filenameInput.length; i++) {
                buffer[i] = null;
                inputFile[i] = new File(filenameInput[i]);
                if (inputFile[i].canRead()) {
                    accessToFile[i] = true;
                }else{
                    System.out.println("Ошибка при открытии файла \"" + inputFile[i] + "\", пропуск.");
                    accessToFile[i] = false;
                }
            }
            for (int i = 0; i < filenameInput.length; i++) {
                if (accessToFile[i]) {
                    scanners[i] = new Scanner(inputFile[i]);
                    buffer[i] = scanners[i].nextLine();
                }
            }

            int value = 0;
            while (true) {
                value = compare(buffer);
                result += buffer[value] + "\n";
                if(result.length() >= MAX_LENGTH_STRING){
                    out.write(result);
                    result = "";
                }
                String lastValue = buffer[value];
                buffer[value] = nextValue(scanners, value, lastValue, buffer);

                boolean flag_exit = true;
                for (int i = 0; i < scanners.length; i++) {
                    if (accessToFile[i]) {
                        if (scanners[i].hasNext()) {
                            flag_exit = false;
                            break;
                        }
                    }
                }
                if (flag_exit) {
                    while (true) {
                        value = compare(buffer);
                        if (buffer[value] != null) {
                            result += buffer[value] + "\n";
                            buffer[value] = null;
                        } else {
                            break;
                        }
                    }
                    break;
                }
            }
            out.write(result);
            out.flush();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
